package com.example.data.mapper;


import com.example.data.network.models.Employee;
import com.example.domain.entity.Entity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Sergey Chilingaryan on 3/31/19.
 */
public class ResponseToEntityMapper {

    private static String getUUID(String s) {
        UUID uid = UUID.nameUUIDFromBytes(s.getBytes());
        return uid.toString();
    }

    public static class EmployeeMapper {

        public static Entity.Employee map(Employee employeeResponse) {

            String id = getUUID(employeeResponse.getAvatrUrl() + employeeResponse.getFirstName() + employeeResponse.getLastName());

            return new Entity.Employee(
                    id,
                    employeeResponse.getBirthday(),
                    employeeResponse.getAvatrUrl(),
                    SpecialtyMapper.mapList(employeeResponse.getSpecialtyList()),
                    employeeResponse.getLastName(),
                    employeeResponse.getFirstName());
        }

        public static List<Entity.Employee> mapList(List<Employee> employeeList) {

            ArrayList<Entity.Employee> employeeArrayList = new ArrayList<>();

            for (Employee employee : employeeList) {
                Entity.Employee employeeEntitiy = map(employee);
                employeeArrayList.add(employeeEntitiy);
            }

            return employeeArrayList;
        }
    }

    public static class SpecialtyMapper {

        public static Entity.Specialty map(Employee.Specialty specialtyData) {
            return new Entity.Specialty(specialtyData.getSpecialtyId(), specialtyData.getName());
        }

        public static List<Entity.Specialty> mapList(List<Employee.Specialty> listSpecialtyData) {

            ArrayList<Entity.Specialty> specialtyEntities = new ArrayList<>();

            for (Employee.Specialty specialty : listSpecialtyData) {
                Entity.Specialty specialtyEntity = map(specialty);
                specialtyEntities.add(specialtyEntity);
            }

            return specialtyEntities;
        }
    }


}
