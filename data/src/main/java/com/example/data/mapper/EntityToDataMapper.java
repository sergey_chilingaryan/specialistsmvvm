package com.example.data.mapper;

import com.example.data.db.employee.EmployeeData;
import com.example.domain.entity.Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey Chilingaryan on 3/31/19.
 */
public class EntityToDataMapper {

    public static class EmployeeMapper {
        public static EmployeeData map(Entity.Employee employeeEntity) {
            EmployeeData employeeData = new EmployeeData();
            employeeData.setAvatrUrl(employeeEntity.avatrUrl);
            employeeData.setBirthday(employeeEntity.birthday);
            employeeData.setSpecialtyList(SpecialtyMapper.mapList(employeeEntity.specialtyList));
            employeeData.setFirstName(employeeEntity.firstName);
            employeeData.setLastName(employeeEntity.lastName);
            employeeData.setId(employeeEntity.id);

            return employeeData;
        }

        public static List<EmployeeData> mapList(List<Entity.Employee> employeeList) {

            ArrayList<EmployeeData> listEmployeeData = new ArrayList<>();

            for (Entity.Employee employee : employeeList) {
                EmployeeData employeeData = map(employee);
                listEmployeeData.add(employeeData);
            }

            return listEmployeeData;
        }
    }

    public static class SpecialtyMapper {
        public static EmployeeData.Specialty map(Entity.Specialty specialtyEntity) {
            EmployeeData.Specialty specialty = new EmployeeData.Specialty();
            specialty.setSpecialtyId(specialtyEntity.id);
            specialty.setName(specialtyEntity.name);
            return specialty;
        }

        public static List<EmployeeData.Specialty> mapList(List<Entity.Specialty> entitySpecialties) {

            ArrayList<EmployeeData.Specialty> listSpecialtyData = new ArrayList<>();

            for (Entity.Specialty entitySpecialty : entitySpecialties) {
                EmployeeData.Specialty specialtyData = map(entitySpecialty);
                listSpecialtyData.add(specialtyData);
            }

            return listSpecialtyData;
        }
    }
}
