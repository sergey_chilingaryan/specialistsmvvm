package com.example.data.mapper;

import com.example.data.db.employee.EmployeeData;
import com.example.domain.entity.Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey Chilingaryan on 3/31/19.
 */
public class DataToEntityMapper {


    public static class EmployeeMapper {
        public static Entity.Employee map(EmployeeData employeeData) {
            return new Entity.Employee(
                    employeeData.getId(),
                    employeeData.getBirthday(),
                    employeeData.getAvatrUrl(),
                    SpecialtyMapper.mapList(employeeData.getSpecialtyList()),
                    employeeData.getLastName(),
                    employeeData.getFirstName());
        }

        public static List<Entity.Employee> mapList(List<EmployeeData> employeeDataList) {
            ArrayList<Entity.Employee> employeeArrayList = new ArrayList<>();


            for (EmployeeData employeeData : employeeDataList) {
                Entity.Employee employeeEntity = map(employeeData);
                employeeArrayList.add(employeeEntity);
            }

            return employeeArrayList;

        }
    }


    public static class SpecialtyMapper {
        public static Entity.Specialty map(EmployeeData.Specialty specialtyData) {
            return new Entity.Specialty(specialtyData.getSpecialtyId(), specialtyData.getName());
        }


        public static List<Entity.Specialty> mapList(List<EmployeeData.Specialty> listSpecialtyData) {

            ArrayList<Entity.Specialty> specialtyEntities = new ArrayList<>();

            for (EmployeeData.Specialty specialty : listSpecialtyData) {
                Entity.Specialty specialtyEntity = map(specialty);
                specialtyEntities.add(specialtyEntity);
            }

            return specialtyEntities;
        }
    }
}
