package com.example.data.datasource.employee;

import com.example.data.datasource.BaseDataSource;
import com.example.domain.entity.Entity;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Sergey Chilingaryan on 4/1/19.
 */
public interface EmployeeApiDataSource extends BaseDataSource {
    Single<List<Entity.Employee>> getEmployees();
}
