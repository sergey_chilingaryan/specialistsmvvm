package com.example.data.datasource.employee;

import com.example.data.mapper.ResponseToEntityMapper;
import com.example.data.network.api.EmployeeApi;
import com.example.data.network.models.Employee;
import com.example.domain.entity.Entity;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Sergey Chilingaryan on 4/1/19.
 */
public class EmployeeApiDataSourceImpl implements EmployeeApiDataSource {
    private EmployeeApi api;

    public EmployeeApiDataSourceImpl(EmployeeApi api) {
        this.api = api;
    }

    @Override
    public Single<List<Entity.Employee>> getEmployees() {
        return api.getEmployees()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(employeeBaseResponse -> {
                    List<Employee> employeesList = employeeBaseResponse.getResponse();
                    return ResponseToEntityMapper
                            .EmployeeMapper
                            .mapList(employeesList);
                });
    }


}
