package com.example.data.datasource.employee;

import com.example.data.commons.InsertFinishedCallback;
import com.example.data.db.employee.EmployeeDao;
import com.example.data.db.employee.EmployeeData;
import com.example.data.mapper.DataToEntityMapper;
import com.example.data.mapper.EntityToDataMapper;
import com.example.domain.entity.Entity;

import java.util.List;
import java.util.concurrent.Executor;

import io.reactivex.Single;

/**
 * Created by Sergey Chilingaryan on 4/1/19.
 */
public class EmployeeDatabaseDataSourceImpl implements EmployeeDatabaseDataSource {

    EmployeeDao employeeDao;
    Executor ioExecutor;


    public EmployeeDatabaseDataSourceImpl(EmployeeDao employeeDao, Executor ioExecutor) {
        this.employeeDao = employeeDao;
        this.ioExecutor = ioExecutor;
    }

    @Override
    public Single<List<Entity.Employee>> getEmployees() {
        return employeeDao.getEmployees().map(DataToEntityMapper.EmployeeMapper::mapList);
    }

    @Override
    public void persist(List<Entity.Employee> employees, InsertFinishedCallback insertFinishedCallback) {
        ioExecutor.execute(() -> {
            List<EmployeeData> employeeData = EntityToDataMapper.EmployeeMapper.mapList(employees);
            employeeDao.insert(employeeData);
            insertFinishedCallback.onFinished();
        });
    }

}
