package com.example.data.network.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sergey Chilingaryan on 3/31/19.
 */
public class Employee {
    @SerializedName("birthday")
    private String birthday;

    @SerializedName("avatr_url")
    private String avatrUrl;

    @SerializedName("specialty")
    private List<Specialty> specialtyList;

    @SerializedName("l_name")
    private String lastName;

    @SerializedName("f_name")
    private String firstName;

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAvatrUrl() {
        return avatrUrl;
    }

    public void setAvatrUrl(String avatrUrl) {
        this.avatrUrl = avatrUrl;
    }

    public List<Specialty> getSpecialtyList() {
        return specialtyList;
    }

    public void setSpecialtyList(List<Specialty> specialtyList) {
        this.specialtyList = specialtyList;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public static class Specialty {
        @SerializedName("specialty_id")
        private int specialtyId;

        @SerializedName("name")
        private String name;

        public int getSpecialtyId() {
            return specialtyId;
        }

        public void setSpecialtyId(int specialtyId) {
            this.specialtyId = specialtyId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
