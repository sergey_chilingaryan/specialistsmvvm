package com.example.data.network.api;

import com.example.data.network.models.BaseResponse;
import com.example.data.network.models.Employee;

import io.reactivex.Single;
import retrofit2.http.GET;

/**
 * Created by Sergey Chilingaryan on 3/31/19.
 */
public interface EmployeeApi {
    @GET("65gb/static/raw/master/testTask.json")
    Single<BaseResponse<Employee>> getEmployees();
}
