package com.example.data.network.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey Chilingaryan on 3/31/19.
 */
public class BaseResponse<T> {
    @SerializedName("response")
    private List<T> response = new ArrayList<>();

    public List<T> getResponse() {
        return response;
    }
}
