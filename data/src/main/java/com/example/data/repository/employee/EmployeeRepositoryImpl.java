package com.example.data.repository.employee;

import com.example.data.datasource.employee.EmployeeApiDataSource;
import com.example.data.datasource.employee.EmployeeDatabaseDataSource;
import com.example.domain.common.ResultState;
import com.example.domain.entity.Entity;
import com.example.domain.repository.emloyee.EmployeeRepository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Sergey Chilingaryan on 4/1/19.
 */
public class EmployeeRepositoryImpl implements EmployeeRepository {
    EmployeeApiDataSource apiDataSource;
    EmployeeDatabaseDataSource databaseDataSource;

    public EmployeeRepositoryImpl(EmployeeApiDataSource employeeApiDataSource, EmployeeDatabaseDataSource employeeDatabaseDataSource) {
        this.apiDataSource = employeeApiDataSource;
        this.databaseDataSource = employeeDatabaseDataSource;
    }

    @Override
    public Flowable<ResultState<List<Entity.Employee>>> getEmployees() {
        return apiDataSource.getEmployees()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .map(employees -> {
                    if (employees.size() > 0) {

                        databaseDataSource.persist(employees, () -> {
                            
                        });

                        return new ResultState.Success<>(employees);
                    } else {
                        return new ResultState.Loading<List<Entity.Employee>>(null);
                    }
                })
                .onErrorReturn(throwable -> new ResultState.Error<>(null, throwable))
                .toFlowable();
    }
}
