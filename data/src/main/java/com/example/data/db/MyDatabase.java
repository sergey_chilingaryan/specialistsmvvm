package com.example.data.db;

import com.example.data.db.converters.SpecialtyConverter;
import com.example.data.db.employee.EmployeeDao;
import com.example.data.db.employee.EmployeeData;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

/**
 * Created by Sergey Chilingaryan on 3/31/19.
 */
@Database(entities = {EmployeeData.class}, version = 1, exportSchema = false)
@TypeConverters({SpecialtyConverter.class})
abstract public class MyDatabase extends RoomDatabase {

    public abstract EmployeeDao employeeDao();
}
