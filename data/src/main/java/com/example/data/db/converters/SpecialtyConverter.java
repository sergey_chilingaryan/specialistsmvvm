package com.example.data.db.converters;

import com.example.data.db.employee.EmployeeData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import androidx.room.TypeConverter;

/**
 * Created by Sergey Chilingaryan on 3/31/19.
 */
public class SpecialtyConverter {

    @TypeConverter
    public static List<EmployeeData.Specialty> jsonToList(String value) {
        Type listType = new TypeToken<List<EmployeeData.Specialty>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String listToJson(List<EmployeeData.Specialty> list) {
        Gson gson = new Gson();
        return gson.toJson(list);
    }
}