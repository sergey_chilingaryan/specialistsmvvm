package com.example.data.db.employee;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by Sergey Chilingaryan on 3/31/19.
 */
@Entity(tableName = "specialists_entity")
public class EmployeeData {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    String id = "";

    @ColumnInfo(name = "birthday")
    String birthday = "";

    @ColumnInfo(name = "avatr_url")
    String avatrUrl;

    @ColumnInfo(name = "specialtyList")
    List<Specialty> specialtyList;

    @ColumnInfo(name = "l_name")
    String lastName;

    @ColumnInfo(name = "f_name")
    String firstName;

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAvatrUrl() {
        return avatrUrl;
    }

    public void setAvatrUrl(String avatrUrl) {
        this.avatrUrl = avatrUrl;
    }

    public List<Specialty> getSpecialtyList() {
        return specialtyList;
    }

    public void setSpecialtyList(List<Specialty> specialtyList) {
        this.specialtyList = specialtyList;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public static class Specialty extends EmployeeData {
        @ColumnInfo(name = "specialty_id")
        int specialtyId;

        @ColumnInfo(name = "name")
        String name;

        public int getSpecialtyId() {
            return specialtyId;
        }

        public void setSpecialtyId(int specialtyId) {
            this.specialtyId = specialtyId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
