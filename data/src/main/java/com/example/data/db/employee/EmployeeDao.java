package com.example.data.db.employee;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import io.reactivex.Single;

/**
 * Created by Sergey Chilingaryan on 3/31/19.
 */
@Dao
public interface EmployeeDao {
    @Query("SELECT * FROM specialists_entity")
    Single<List<EmployeeData>> getEmployees();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<EmployeeData> employeeData);
}
