package com.example.data.commons;

/**
 * Created by Sergey Chilingaryan on 4/1/19.
 */
@FunctionalInterface
public interface InsertFinishedCallback {
    void onFinished();
}
