package com.example.specialistsmvvm;

import android.app.Application;
import android.content.Context;

import com.example.specialistsmvvm.di.DaggerAppComponent;

import androidx.multidex.MultiDex;
import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

/**
 * Created by Sergey Chilingaryan on 4/1/19.
 */
public class MyApplication extends DaggerApplication {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().create(this);
    }
}
