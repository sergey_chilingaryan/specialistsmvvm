package com.example.specialistsmvvm.di;

import android.content.Context;

import com.example.specialistsmvvm.MyApplication;
import com.example.specialistsmvvm.di.qualifiers.ApplicationContext;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Sergey Chilingaryan on 4/1/19.
 */

@Module(includes = {NetModule.class, DbModule.class})
abstract class AppModule {
    @ApplicationContext
    @Binds
    abstract Context provideApplicationContext(MyApplication myApplication);
}
