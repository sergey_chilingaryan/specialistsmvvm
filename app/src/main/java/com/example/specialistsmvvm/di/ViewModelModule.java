package com.example.specialistsmvvm.di;

import com.example.presentation.ui.specialty.SpecialtyViewModel;
import com.example.specialistsmvvm.di.qualifiers.ViewModelKey;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created by Sergey Chilingaryan on 4/1/19.
 */
@Module
public abstract class ViewModelModule {

    @Binds
    public abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);

    @Binds
    @IntoMap
    @ViewModelKey(SpecialtyViewModel.class)
    public abstract ViewModel bindSpecialtyViewModel(SpecialtyViewModel viewModel);
}
