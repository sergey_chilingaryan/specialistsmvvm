package com.example.specialistsmvvm.di;

import com.example.specialistsmvvm.MyApplication;
import com.example.specialistsmvvm.di.specialty.SpecialtyModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by Sergey Chilingaryan on 4/1/19.
 */
@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        ViewModelModule.class,
        AppModule.class,
        MainModule.class,
        SpecialtyModule.class
})

public interface AppComponent extends AndroidInjector<MyApplication> {
    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<MyApplication> {
    }
}
