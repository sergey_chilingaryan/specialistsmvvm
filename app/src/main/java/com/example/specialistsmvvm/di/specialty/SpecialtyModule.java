package com.example.specialistsmvvm.di.specialty;

import com.example.data.datasource.employee.EmployeeApiDataSource;
import com.example.data.datasource.employee.EmployeeApiDataSourceImpl;
import com.example.data.datasource.employee.EmployeeDatabaseDataSource;
import com.example.data.datasource.employee.EmployeeDatabaseDataSourceImpl;
import com.example.data.db.employee.EmployeeDao;
import com.example.data.network.api.EmployeeApi;
import com.example.data.repository.employee.EmployeeRepositoryImpl;
import com.example.domain.repository.emloyee.EmployeeRepository;
import com.example.domain.usecase.employee.GetEmployeesUseCase;
import com.example.domain.usecase.employee.GetEmployeesUseCaseImpl;

import java.util.concurrent.Executors;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Sergey Chilingaryan on 4/1/19.
 */
@Module
public class SpecialtyModule {
    @Provides
    public EmployeeDatabaseDataSource provideDatabaseSource(EmployeeDao employeeDao) {
        return new EmployeeDatabaseDataSourceImpl(employeeDao, Executors.newSingleThreadExecutor());
    }

    @Provides
    public EmployeeApiDataSource provideApiSource(EmployeeApi employeeApi) {
        return new EmployeeApiDataSourceImpl(employeeApi);
    }

    @Provides
    public EmployeeRepository provideRepository(EmployeeApiDataSource apiDataSource, EmployeeDatabaseDataSource databaseDataSource) {
        return new EmployeeRepositoryImpl(apiDataSource, databaseDataSource);
    }

    @Provides
    public GetEmployeesUseCase provideGetEmployeesUseCase(EmployeeRepository repository) {
        return new GetEmployeesUseCaseImpl(repository);
    }
}
