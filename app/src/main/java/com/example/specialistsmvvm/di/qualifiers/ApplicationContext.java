package com.example.specialistsmvvm.di.qualifiers;

import java.lang.annotation.Retention;

import javax.inject.Qualifier;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Sergey Chilingaryan on 4/1/19.
 */

@Qualifier
@Retention(RUNTIME)
public @interface ApplicationContext {
}