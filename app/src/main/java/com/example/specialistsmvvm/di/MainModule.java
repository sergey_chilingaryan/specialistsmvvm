package com.example.specialistsmvvm.di;

import com.example.presentation.MainActivity;
import com.example.specialistsmvvm.di.specialty.SpecialtyFragmentModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sergey Chilingaryan on 4/1/19.
 */

@Module(includes = {SpecialtyFragmentModule.class})
abstract class MainModule {
    @ContributesAndroidInjector
    public abstract MainActivity get();
}
