package com.example.specialistsmvvm.di;

import android.content.Context;

import com.example.data.db.MyDatabase;
import com.example.data.db.employee.EmployeeDao;
import com.example.specialistsmvvm.di.qualifiers.ApplicationContext;

import javax.inject.Singleton;

import androidx.room.Room;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Sergey Chilingaryan on 4/1/19.
 */

@Module
public class DbModule {

    @Singleton
    @Provides
    public MyDatabase provideMyDatabase(@ApplicationContext Context context) {
        return Room.databaseBuilder(context, MyDatabase.class, "mydatabase").build();
    }

    @Singleton
    @Provides
    public EmployeeDao provideEmployeeDao(MyDatabase database) {
        return database.employeeDao();
    }
}
