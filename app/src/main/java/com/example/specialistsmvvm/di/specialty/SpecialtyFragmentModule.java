package com.example.specialistsmvvm.di.specialty;

import com.example.presentation.ui.specialty.SpecialtyListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sergey Chilingaryan on 4/1/19.
 */
@Module
public abstract class SpecialtyFragmentModule {
    @ContributesAndroidInjector
    public abstract SpecialtyListFragment specialtyListFragment();
}
