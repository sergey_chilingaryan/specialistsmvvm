package com.example.presentation.ui.base;

import dagger.android.support.DaggerFragment;

/**
 * Created by Sergey Chilingaryan on 4/1/19.
 */
public class BaseFragment extends DaggerFragment {
    public void showLoading() {
    }

    public void hideLoading() {
    }
}
