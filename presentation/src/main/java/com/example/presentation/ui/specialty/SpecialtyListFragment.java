package com.example.presentation.ui.specialty;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.domain.entity.Entity;
import com.example.presentation.R;
import com.example.presentation.ui.base.BaseFragment;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

public class SpecialtyListFragment extends BaseFragment {

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private SpecialtyViewModel specialtyViewModel;
    private RecyclerView specialtyRecyclerView;
    private SpecialtyAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_specialty, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new SpecialtyAdapter();
        adapter.setItemClickListener(this::onListItemClick);

        specialtyRecyclerView = view.findViewById(R.id.specialty_list);
        specialtyRecyclerView.setHasFixedSize(true);
        specialtyRecyclerView.setAdapter(adapter);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        specialtyViewModel = ViewModelProviders.of(this, viewModelFactory).get(SpecialtyViewModel.class);
        specialtyViewModel.liveData.observe(this, specialties -> {
            adapter.setData(specialties);
            adapter.notifyDataSetChanged();
        });

        specialtyViewModel.errorLiveData.observe(this, s -> {
            Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
        });

        specialtyViewModel.getSpecialtyList();
    }

    private void onListItemClick(Entity.Specialty item, int position) {
        Toast.makeText(getContext(), "go to employee list", Toast.LENGTH_SHORT).show();
    }

}
