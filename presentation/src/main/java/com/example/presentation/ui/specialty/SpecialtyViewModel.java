package com.example.presentation.ui.specialty;

import com.example.domain.common.ResultState;
import com.example.domain.entity.Entity;
import com.example.domain.usecase.employee.GetEmployeesUseCase;
import com.example.presentation.ui.base.BaseViewModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.MutableLiveData;
import io.reactivex.disposables.Disposable;

/**
 * Created by Sergey Chilingaryan on 4/1/19.
 */
public class SpecialtyViewModel extends BaseViewModel {

    public MutableLiveData<List<Entity.Specialty>> liveData = new MutableLiveData<>();
    public MutableLiveData<String> errorLiveData = new MutableLiveData<>();
    GetEmployeesUseCase getEmployeesUseCase;

    @Inject
    public SpecialtyViewModel(GetEmployeesUseCase getEmployeesUseCase) {
        this.getEmployeesUseCase = getEmployeesUseCase;
    }

    public void getSpecialtyList() {
        Disposable d = getEmployeesUseCase.getEmployees()
                .subscribe(listResultState -> {
                    if (listResultState instanceof ResultState.Success) {

                        HashSet<Entity.Specialty> hs = new HashSet<>();
                        List<Entity.Employee> data = listResultState.data;
                        for (Entity.Employee employee : data) {
                            hs.addAll(employee.specialtyList);
                        }

                        ArrayList<Entity.Specialty> specialties = new ArrayList<>(hs);
                        liveData.postValue(specialties);
                    } else {
                        errorLiveData.postValue("somethign went wrong");
                    }
                });
        track(d);
    }
}
