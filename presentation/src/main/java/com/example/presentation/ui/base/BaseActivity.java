package com.example.presentation.ui.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by Sergey Chilingaryan on 4/1/19.
 */
public abstract class BaseActivity extends DaggerAppCompatActivity {

    NavController navController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onStart() {
        super.onStart();
        navController = Navigation.findNavController(this, getNavControllerId());
    }

    @Override
    public boolean onSupportNavigateUp() {
        navController.navigateUp();
        return super.onSupportNavigateUp();

    }

    public abstract int getNavControllerId();

}
