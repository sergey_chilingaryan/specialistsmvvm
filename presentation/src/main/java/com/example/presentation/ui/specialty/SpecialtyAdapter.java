package com.example.presentation.ui.specialty;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.domain.entity.Entity;
import com.example.presentation.ItemClickListener;
import com.example.presentation.R;
import com.example.presentation.databinding.ItemSpecialtyBinding;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class SpecialtyAdapter extends RecyclerView.Adapter<SpecialtyAdapter.ViewHolder> {
    private ItemClickListener<Entity.Specialty> itemClickListener;

    private List<Entity.Specialty> mData = new ArrayList<>();

    public SpecialtyAdapter() {

    }

    public void setItemClickListener(ItemClickListener<Entity.Specialty> itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void setData(List<Entity.Specialty> mData) {
        this.mData = mData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int pos) {
        ItemSpecialtyBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_specialty, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int pos) {
        viewHolder.bindTo(mData.get(pos), pos);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ItemSpecialtyBinding binding;

        public ViewHolder(ItemSpecialtyBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindTo(Entity.Specialty item, int pos) {
            binding.setSpecialtyEntity(item);
            binding.executePendingBindings();
            itemView.setOnClickListener(v -> {
                if (itemClickListener != null) {
                    itemClickListener.onItemClick(item, pos);
                }
            });
        }
    }

}