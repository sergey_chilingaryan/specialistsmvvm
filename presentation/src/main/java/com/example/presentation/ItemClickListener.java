package com.example.presentation;

@FunctionalInterface
public interface ItemClickListener<TYPE> {
    void onItemClick(TYPE item, int position);
}