package com.example.presentation;

import android.os.Bundle;

import com.example.presentation.ui.base.BaseActivity;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public int getNavControllerId() {
        return R.id.activityMainHomeHostFragment;
    }
}
