package com.example.domain.usecase.employee;

import com.example.domain.common.ResultState;
import com.example.domain.entity.Entity;
import com.example.domain.usecase.BaseUseCase;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Sergey Chilingaryan on 3/31/19.
 */
public interface GetEmployeesUseCase extends BaseUseCase {
    Flowable<ResultState<List<Entity.Employee>>> getEmployees();
}
