package com.example.domain.usecase.employee;

import com.example.domain.common.ResultState;
import com.example.domain.entity.Entity;
import com.example.domain.repository.emloyee.EmployeeRepository;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Sergey Chilingaryan on 3/31/19.
 */
public class GetEmployeesUseCaseImpl implements GetEmployeesUseCase {

    private EmployeeRepository repository;

    public GetEmployeesUseCaseImpl(EmployeeRepository repository) {
        this.repository = repository;
    }

    @Override
    public Flowable<ResultState<List<Entity.Employee>>> getEmployees() {
        return repository.getEmployees();
    }
}
