package com.example.domain.common;

/**
 * Created by Sergey Chilingaryan on 3/31/19.
 */
public class ResultState<T> {
    public T data;

    public ResultState(T data) {
        this.data = data;
    }

    public static class Loading<T> extends ResultState<T> {
        public Loading(T data) {
            super(data);
        }
    }

    public static class Error<T> extends ResultState<T> {
        private Throwable throwable;

        public Error(T data, Throwable throwable) {
            super(data);
            this.throwable = throwable;
        }
    }

    public static class Success<T> extends ResultState<T> {
        public Success(T data) {
            super(data);
        }
    }
}
