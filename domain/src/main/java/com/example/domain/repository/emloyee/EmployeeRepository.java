package com.example.domain.repository.emloyee;

import com.example.domain.common.ResultState;
import com.example.domain.entity.Entity;
import com.example.domain.repository.BaseRepository;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Sergey Chilingaryan on 3/31/19.
 */
public interface EmployeeRepository extends BaseRepository {
    Flowable<ResultState<List<Entity.Employee>>> getEmployees();
}
