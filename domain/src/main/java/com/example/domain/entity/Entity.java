package com.example.domain.entity;

import com.example.domain.common.utils.ObjectsCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey Chilingaryan on 3/31/19.
 */
public class Entity {

    public static class Employee extends Entity {
        public String id;
        public String birthday;
        public String avatrUrl;
        public List<Specialty> specialtyList = new ArrayList<>();
        public String lastName;
        public String firstName;

        public Employee() {

        }

        public Employee(String id, String birthday, String avatrUrl, List<Specialty> specialtyList, String lastName, String firstName) {
            this.id = id;
            this.birthday = birthday;
            this.avatrUrl = avatrUrl;
            this.specialtyList = specialtyList;
            this.lastName = lastName;
            this.firstName = firstName;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Employee employee = (Employee) o;
            return ObjectsCompat.equals(id, employee.id) &&
                    ObjectsCompat.equals(birthday, employee.birthday) &&
                    ObjectsCompat.equals(avatrUrl, employee.avatrUrl) &&
                    ObjectsCompat.equals(lastName, employee.lastName) &&
                    ObjectsCompat.equals(firstName, employee.firstName);
        }

        @Override
        public int hashCode() {
            return ObjectsCompat.hash(id, birthday, avatrUrl, lastName, firstName);
        }

    }

    public static class Specialty extends Entity {
        public int id;
        public String name;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Specialty specialty = (Specialty) o;
            return id == specialty.id;
        }

        @Override
        public int hashCode() {
            return ObjectsCompat.hash(id);
        }

        public Specialty() {
        }

        public Specialty(int id, String name) {
            this.id = id;
            this.name = name;
        }
    }
}
